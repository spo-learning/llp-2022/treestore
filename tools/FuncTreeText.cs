﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DrGenerator
{
    internal interface ITextFuncTreeNodeVisitor<T>
    {
        T VisitSymbol(TextFuncTreeSymbol symbol);
        T VisitLiteral(TextFuncTreeLiteral literal);
        T VisitApply(TextFuncTreeApply apply);
    }

    internal abstract class TextFuncTreeNode
    {
        public bool ForceNewLineBefore { get; set; }
        public bool ForceNewLineAfter { get; set; }

        public T Apply<T>(ITextFuncTreeNodeVisitor<T> visitor)
        {
            return this.ApplyImpl<T>(visitor);
        }

        protected abstract T ApplyImpl<T>(ITextFuncTreeNodeVisitor<T> visitor);

        public static implicit operator TextFuncTreeNode(string str) { return new TextFuncTreeLiteral(ValueKind.String, str); }

        public static implicit operator TextFuncTreeNode(int value) { return new TextFuncTreeLiteral(ValueKind.Int, value.ToString()); }

        public static implicit operator TextFuncTreeNode(bool value) { return new TextFuncTreeLiteral(ValueKind.Bool, value.ToString()); }

        public static implicit operator TextFuncTreeNode(DateTime value) { return new TextFuncTreeLiteral(ValueKind.DateTime, value.ToString(CultureInfo.InvariantCulture.DateTimeFormat)); }
    }

    internal class TextFuncTreeSymbol : TextFuncTreeNode
    {
        public string Name { get; private set; }

        public TextFuncTreeSymbol(string name)
        {
            this.Name = name;
        }

        protected override T ApplyImpl<T>(ITextFuncTreeNodeVisitor<T> visitor)
        {
            return visitor.VisitSymbol(this);
        }
    }

    internal class TextFuncTreeLiteral : TextFuncTreeNode
    {
        public ValueKind Kind { get; private set; }
        public string Value { get; private set; }

        public TextFuncTreeLiteral(ValueKind kind, string value)
        {
            this.Kind = kind;
            this.Value = value;
        }

        protected override T ApplyImpl<T>(ITextFuncTreeNodeVisitor<T> visitor)
        {
            return visitor.VisitLiteral(this);
        }
    }

    internal class TextFuncTreeApply : TextFuncTreeNode
    {
        public string Head { get; private set; }
        public ReadOnlyCollection<TextFuncTreeNode> Args { get; private set; }

        public TextFuncTreeApply(string headName, params TextFuncTreeNode[] args)
        {
            this.Head = headName;
            this.Args = new ReadOnlyCollection<TextFuncTreeNode>(args);
        }

        protected override T ApplyImpl<T>(ITextFuncTreeNodeVisitor<T> visitor)
        {
            return visitor.VisitApply(this);
        }
    }

    internal static class TextFuncTree
    {
        private enum TokenKind
        {
            Name,
            Value,
            ListSeparator,
            OpenBrace,
            CloseBrace,
            Whitespace
        }

        private static readonly Dictionary<TokenKind, string> _petternByTokenKind = new Dictionary<TokenKind, string>() {
            { TokenKind.Value, "(True)|(False)|([0-9]*\\.[0-9]+)|([0-9]+)|(\\\"[^\\\"]*\\\")" },
            { TokenKind.Name, "[a-zA-Z_][a-zA-Z_0-9]*" },
            { TokenKind.ListSeparator, @"\," },
            { TokenKind.CloseBrace, @"[\)\]]" },
            { TokenKind.OpenBrace, @"[\(\[]" },
            { TokenKind.Whitespace, @"[\ \t\r\n]" },
        };
        private static readonly string _tokensPattern = string.Join("|", _petternByTokenKind.Select(
                kv => $"(?<{kv.Key}>({kv.Value}))"
            ));
        private static readonly Regex _tokenizer = new Regex("^(" + _tokensPattern + ")*$");

        public static TextFuncTreeNode Parse(string text)
        {
            var match = _tokenizer.Match(text);
            if (match.Success && match.Length == text.Length)
            {
                var tokens = Enum.GetValues(typeof(TokenKind)).OfType<TokenKind>().Except(new[] { TokenKind.Whitespace }).SelectMany(
                    k => match.Groups[k.ToString()].Captures.OfType<Capture>()
                              .Select(c => new KeyValuePair<TokenKind, Capture>(k, c))
                ).OrderBy(kv => kv.Value.Index).ToArray();

                var i = 0;
                var tree = ParseImpl(tokens, ref i);
                if (i < tokens.Length)
                    throw new ApplicationException("Error at position " + tokens[i].Value.Index);

                return tree;
            }
            else
            {
                throw new ApplicationException("Error at position " + match.Length);
            }
        }

        private static TextFuncTreeNode ParseImpl(KeyValuePair<TokenKind, Capture>[] tokens, ref int i)
        {
            if (i + 1 > tokens.Length)
                throw new ApplicationException();

            var t = tokens[i++];
            switch (t.Key)
            {
                case TokenKind.Name:
                    break;
                case TokenKind.Value:
                    {
                        var s = t.Value.Value;
                        ValueKind kind;

                        if (int.TryParse(s, out var ivalue))
                            kind = ValueKind.Int;
                        else if (bool.TryParse(s, out var bvalue))
                            kind = ValueKind.Bool;
                        else if (DateTime.TryParse(s, CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.None, out var dtvalue))
                            kind = ValueKind.DateTime;
                        else if (s.StartsWith("\"") && s.EndsWith("\"") && s.Length > 1)
                        {
                            kind = ValueKind.String;
                            s = s.Substring(1, s.Length - 2);
                        }
                        else
                            throw new NotSupportedException("Unsupported kind of number represented as fllowing string: " + s);

                        return new TextFuncTreeLiteral(kind, s);

                    }
                case TokenKind.ListSeparator:
                case TokenKind.OpenBrace:
                case TokenKind.CloseBrace:
                case TokenKind.Whitespace:
                default:
                    throw new ApplicationException();
            }

            if (i >= tokens.Length || tokens[i].Key != TokenKind.OpenBrace)
                return new TextFuncTreeSymbol(t.Value.Value);

            var name = t.Value.Value;
            var args = new List<TextFuncTreeNode>();

            i++;
            if (tokens[i].Key != TokenKind.CloseBrace)
            {
                args.Add(ParseImpl(tokens, ref i));
                if (i >= tokens.Length)
                    throw new ApplicationException("Unexpected end of expression");

                while (tokens[i].Key == TokenKind.ListSeparator)
                {
                    i++;
                    args.Add(ParseImpl(tokens, ref i));

                    if (i >= tokens.Length)
                        throw new ApplicationException("Unexpected end of expression");
                }

                if (tokens[i].Key != TokenKind.CloseBrace)
                    throw new ApplicationException("Malformed expression at position " + tokens[i].Value.Index);
            }

            i++;
            return new TextFuncTreeApply(name, args.ToArray());
        }

        public static TextFuncTreeNode Apply(this string name, params TextFuncTreeNode[] args)
        {
            return new TextFuncTreeApply(name, args);
        }

    }

    internal class FuncTreePrinter : ITextFuncTreeNodeVisitor<object>
    {
        private readonly TextWriter _writer;
        private int _col = 0;

        public FuncTreePrinter(TextWriter writer)
        {
            _writer = writer;
        }

        public void Print(TextFuncTreeNode node)
        {
            node.Apply(this);
            _writer.WriteLine();
            _col = 0;
        }

        public void Print(IEnumerable<TextFuncTreeNode> nodes)
        {
            _writer.WriteLine();

            foreach (var item in nodes)
                this.Print(item);

            _writer.WriteLine();
        }

        private void Write(string str)
        {
            _col += str.Length;
            _writer.Write(str);
        }

        private readonly Stack<int> _indents = new Stack<int>();

        private void CaptureIndent()
        {
            _indents.Push(_col);
        }

        private void ReleaseIndent()
        {
            _indents.Pop();
        }

        private void NewLineIfNeededBefore(TextFuncTreeNode node)
        {
            if (node?.ForceNewLineBefore == true)
            {
                _writer.WriteLine();
                _writer.Write(new string(' ', _indents.Peek()));
                _col = _indents.Peek();
            }
        }

        private void NewLineIfNeededAfter(TextFuncTreeNode node)
        {
            if (node?.ForceNewLineAfter == true)
            {
                _writer.WriteLine();
                _writer.Write(new string(' ', _indents.Peek()));
                _col = _indents.Peek();
            }
        }

        object ITextFuncTreeNodeVisitor<object>.VisitApply(TextFuncTreeApply operation)
        {
            this.Write(operation.Head);
            this.Write("(");

            var it = operation.Args.GetEnumerator();
            TextFuncTreeNode prev = null;
            this.CaptureIndent();
            if (it.MoveNext())
            {
                prev = it.Current;
                this.NewLineIfNeededBefore(it.Current);
                it.Current.Apply(this);
                while (it.MoveNext())
                {
                    this.Write(", ");
                    this.NewLineIfNeededAfter(prev);
                    this.NewLineIfNeededBefore(it.Current);
                    it.Current.Apply(this);
                    prev = it.Current;
                }
            }

            this.Write(")");
            this.NewLineIfNeededAfter(prev);
            this.ReleaseIndent();

            return null;
        }

        object ITextFuncTreeNodeVisitor<object>.VisitLiteral(TextFuncTreeLiteral value)
        {
            if (value.Kind == ValueKind.String)
            {
                this.Write("\"");
                this.Write(value.Value);
                this.Write("\"");
            }
            else
            {
                this.Write(value.Value);
            }

            return null;
        }

        object ITextFuncTreeNodeVisitor<object>.VisitSymbol(TextFuncTreeSymbol variable)
        {
            this.Write(variable.Name);
            return null;
        }
    }
}

