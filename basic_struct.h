#include <utility>
#include <cstdint>
#include <string>

//
// Created by maks on 01.11.22.
//
#ifndef TREESTORE_BASIC_STRUCT_H
#define TREESTORE_BASIC_STRUCT_H

typedef enum type_cell {
    INT = 0,
    DOUBLE,
    STR,
    BOOL,
} type_cell;

typedef struct cell {
    type_cell type;
    int32_t size;
    void* value;
} cell;

typedef enum type_query {
    CREAT = 0,
    READ,
    UPDATE,
    DELETE,
} type_query;

typedef struct query {
    type_query type;
    std::string path;
    cell* query_value = nullptr;
} query;

#endif //TREESTORE_BASIC_STRUCT_H
