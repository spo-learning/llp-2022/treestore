//
// Created by maks on 01.11.22.
//
#ifndef TREESTORE_BASIC_STRUCT_H
#define TREESTORE_BASIC_STRUCT_H

#include <utility>
#include <cstdint>
#include <string>
#include <vector>
#include <unordered_map>


typedef enum {
    INT = 0,
    DOUBLE,
    STR,
    BOOL,
    FK,
} type_cell;

typedef struct cell {

    typedef union {
        uint32_t int_value;
        double double_value;
        std::string string_value;
        bool bool_value;
        std::pair<std::string, std::string> fk_value; // pair<entity_name(type), >
    } cell_value;

    type_cell type;
    int32_t size;
    cell_value value;

} cell;

typedef struct entity {
    std::vector<type_cell> types;
} entity;

typedef struct schema {
    std::unordered_map<std::string, entity> entities; // entity_name, entity
} schema;


typedef enum {
    CREAT = 0,
    READ,
    UPDATE,
    DELETE,
} type_query;

typedef struct query {
    type_query type;
    std::string path;
    cell &query_value;
} query;

#endif //TREESTORE_BASIC_STRUCT_H
